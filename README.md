# Boiler

Boiler is a command line focused bash script used to quickly generate files based off of templates found in a central database. Besides generating files, users can manipulate the database with options such as add, import, delete, and update. It aims to be a productivity booster while writing code with editors such as Vim, Neovim, Nano, etc. Type `boiler -h`&nbsp; after downloading to quickly see the available options.

## Installation

Installation of Boiler from the command line is pretty straightforward: Navigate to the directory you want Boiler and the plates directory to live, and then download it with `git clone`. If you decide to dowload Boiler without the plates directory, it will ask
you if you want to create it on first run.

```bash
$ cd /directory/path/ # where boiler and the plates directory will live
$ git clone git@gitlab.com:pixel_cutter/boiler
```

NOTE: Boiler will not run properly until a plates directory is found in its source directory. Choose 'y' when prompted for the creation of the plates directory if you want Boiler to function as intended.  
<b />  
<b />

## Usage  
<b />  
<b />

**Creating a New Template:**&nbsp; `boiler -a [NEW TEMPLATE NAME] [FILE EXTENSION]`

```bash
$ boiler -a jmain java
# Prompts
Enter a Short Description: Standard java main boilerplate
```

A new template named "jmain.plate", containing two fields and template text, will be created in the plates directory. The fields within jmain.plate will be as follows: a file type field associated with "java", and a description field associated with "Standard java main boilerplate". Template text will come from an editor window that opens when a new template is created. Once the editor window closes, Boiler will update the index file (located in the plates directory) with the templates name, file ext and description.

**jmain.plate's Contents**

```txt
FILE_TYPE|java
DESCRIPTION|Standard java main boilerplate
/**/

public class <NAME>
{
        public static void main(String[] args)
        {

        }
}
```

WARNING: Boiler expects 'FILE_TYPE' and 'DESCRIPTION' to be the first two lines of a plate file. If you decide to manually edit a plate file after it has already been created, make sure you keep those lines where they are, only changing the values to the right of the '|' delimiter.  
<b />  
<b />

**Creating a New File With a Template:**&nbsp; `boiler [NEW FILE NAME] [TEMPLATE NAME]`

```bash
$ boiler Dots jmain
```

A new file named "Dots" will be created in the current directory. Dots will contain jmain's contents and will have a '.java' file extension. Use the `-e` option to automatically open up the new file in an editor after creation.  
<b />  
<b />

**Importing Files:**&nbsp; `boiler -i [FILE]...`

```bash
$ boiler -i path/to/cmain.c
# Prompts
Enter new name, or press enter to keep 'cmain':
Enter short description: Standard c main boilerplate
Would you like to edit file before import (y/n): n
```

In the above example, the file 'cmain.c' was converted to the template format (non-destructive) and then added to the plates directory as 'cmain.plate'. cmain.plate will have 'c' associated with the FILE_TYPE field, and 'Standard c main boilerplate' associated with the DESCRIPTION field. More than one file can be imported at a time.  
<b />  
<b />



**Listing Available Templates**

```bash
$ boiler -l
# Output
|NAME|    |FILE TYPE|            |DESCRIPTION|
> cmain        c                  Standard c main boilerplate
> jmain        java               Standard java main boilerplate    
```

To list the available templates, Boiler simply parses and prints out the index file located in the plates directory.  
<b />  
<b />

**Updating the Index File:**&nbsp; `boiler -u` &nbsp; or &nbsp; `boiler -f`

- Use `-u`&nbsp; when a template file has been added to the plates directory without the `-a`&nbsp; or `-i`&nbsp; options.
- Use `-f`&nbsp; when you have manually edited a FILE_TYPE or DESCRIPTION field of a template file, or when the index file has become corrupted.

Both options parse the template files in the plates directory the same way, the only difference being that `-u`&nbsp; will skip template files found in the index file, while `-f`&nbsp; will not skip any template files, making it slower.  
<b />  
<b />

**Deleting Template Files:**&nbsp; `boiler -d [TEMPLATE NAME]....`

```bash
$ boiler -d cmain jmain
```

Both 'cmain' and 'jmain' template files will be deleted from the plates directory and index.  
<b />  
<b />

## Authors

Jared Diamond

